from django.apps import AppConfig


class FrsuperConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'FrSuper'
