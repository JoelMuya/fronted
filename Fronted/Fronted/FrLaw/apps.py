from django.apps import AppConfig


class FrlawConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'FrLaw'
